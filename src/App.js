import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import About from "./Pages/About";
import AboutMore from "./Pages/AboutMore";
import ErrorPage from "./Pages/ErrorPage";
import Home from "./Pages/Home";
import Profile from "./Pages/Profile";
import SingleProfile from "./Pages/SingleProfile";

const App = () => {
  return (
    <BrowserRouter>
      <nav>
        <Link to="/"> Home </Link>
        <Link to="about"> About </Link>
        <Link to="profile"> Profile </Link>
      </nav>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/profile/:username" element={<SingleProfile />} />
        <Route path="/profile" element={<Profile />} />
        {/* <Route path="/about/*" element={<About />} /> */}
        <Route path="/about/" element={<About />}>
          <Route path="more" element={<AboutMore />} />
          <Route path="*" element={<div>Wrong param</div>} />
        </Route>
        <Route path="*" element={<ErrorPage />} />
      </Routes>
      <div> Footer </div>
    </BrowserRouter>
  );
};

export default App;
