import { useNavigate, useParams } from "react-router-dom";

const SingleProfile = () => {
  const navigate = useNavigate();
  const { username } = useParams();
  return (
    <div>
      <p> This is SINGLE PROFILE component for {username} </p>
      <button onClick={() => navigate("/about")}>Change to about page</button>
    </div>
  );
};

export default SingleProfile;
