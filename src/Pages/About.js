import { Outlet, Route, Routes } from "react-router-dom";
import AboutMore from "./AboutMore";

const About = () => {
    return (<>
        <div> This is About component </div>
        <Outlet />
        {/* <Routes>
            <Route path="more" element={<AboutMore />}/>
            <Route path="*" element={<div>Wrong param</div>} />
        </Routes> */}
        
        </>
    )
}

export default About;